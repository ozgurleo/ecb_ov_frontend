const proxy = require("http-proxy-middleware");
module.exports = function(app) {
  app.use(
    "/getAllRatesEu",
    proxy({
      target: "http://192.168.56.102:8081/v1.0",
      changeOrigin: true
    })
  );
  app.use(
    "/getAvgRate",
    proxy({
      target: "http://192.168.56.102:8081/v1.0",
      changeOrigin: true
    })
  );
  app.use(
    "/getInEuro",
    proxy({
      target: "http://192.168.56.102:8081/v1.0",
      changeOrigin: true
    })
  );
  app.use(
    "/getRateEuOnDate",
    proxy({
      target: "http://192.168.56.102:8081/v1.0",
      changeOrigin: true
    })
  );
  app.use(
    "/getToEuro",
    proxy({
      target: "http://192.168.56.102:8081/v1.0",
      changeOrigin: true
    })
  );
  app.use(
    "/readOvChipPDF",
    proxy({
      target: "http://192.168.56.102:8080/v1.0",
      changeOrigin: true
    })
  );
};
