import React, { useState } from "react";
import { CustomInput, Form, FormGroup, Row, Col, Button } from "reactstrap";
import axios from "axios";
import Image from "../repo/Image"

const OvchipCard = () => {
  const [ovFile, setOvFile] = useState(null);
  const [totalAmount, setTotalAmount] = useState(0);
  const fileSelectHandler = event => {
    setOvFile(event.target.files[0]);
  };
  const fileUploadHandler = () => {
    const fd = new FormData();
    fd.append("OvChipPDF", ovFile, ovFile.name);

    axios.post("/readOvChipPDF/", fd).then(res => {
      setTotalAmount(res.data.total);
    });
  };

  return (
    <div>
      <Image />
      <h1 className="mx-5 my-5 text-center ">OV-ChipKaart PDF Reader</h1>
      <Form>
        <FormGroup>
          <Row className="justify-content-md-center">
            <Col sm={3} className="py-5">
              <CustomInput
                type="file"
                id="exampleCustomFileBrowser"
                name="customFile"
                onChange={fileSelectHandler}
              />
            </Col>
            <Col sm={1} className="py-5">
              <Button onClick={fileUploadHandler} color="success">
                Execute
              </Button>
            </Col>
            <Col sm={1} className="py-5 pr-5">
              <Button color="secondary">Clear</Button>
            </Col>
            <Col sm={3} className="py-5 px-5">
              <span>
                Total Amount : <strong>{totalAmount}</strong> EUR
              </span>
            </Col>
          </Row>
        </FormGroup>
      </Form>
    </div>
  );
};

export default OvchipCard;
