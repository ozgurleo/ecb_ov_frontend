import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Image from "../repo/Image";

function Todayrate() {
  const [results, setResults] = useState([]);
  const [date, setDate] = useState("");
  const [optToCurrency, setOptToCurrency] = useState("");
  const [rateCurrency, setRateCurrency] = useState("");
  const [rateEuro, setRateEuro] = useState("");

  useEffect(() => {
    getResults();
  }, []);

  const getResults = async () => {
    const response = await axios.get("/getAllRatesEu/");
    const arrayResult = Object.keys(response.data.rates);
    setResults(arrayResult);
  };

  const dateHandler = event => {
    setDate(event.target.value);
  };

  const getTodayRate = async () => {
    const response = await axios.get(
      `/getRateEuOnDate?currency=${optToCurrency}&date=${date}`
    );
    setRateCurrency(response.data.rate_from.toFixed(3));
    setRateEuro(response.data.rate.toFixed(3));
  };

  const dropDownHandler = event => {
    setOptToCurrency(event.target.value);
  };

  return (
    <div>
      <Image />
      <Container>
        <h1 className="mx-5 my-5 text-center ">Exchange rate</h1>
        <Row className="justify-content-md-center my-5 py-5">
          <Col sm={2}>
            <span>EUR</span>
          </Col>
          <Col sm={1}>
            <Row className="justify-content-md-center">
              <span>{rateEuro}</span>
            </Row>
            <Row className="justify-content-md-center">
              <span
                style={{
                  width: 0,
                  height: 0,
                  borderTop: "10px solid transparent",
                  borderLeft: "10px solid #284457",
                  borderBottom: "10px solid transparent"
                }}
              ></span>
            </Row>
            <Row className="justify-content-md-center">
              <span
                style={{
                  width: 0,
                  height: 0,
                  borderTop: "10px solid transparent",
                  borderRight: "10px solid #284457",
                  borderBottom: "10px solid transparent"
                }}
              ></span>
            </Row>
            <Row>
              <span>{rateCurrency}</span>
            </Row>
          </Col>
          <Col sm={3}>
            <select
              onChange={dropDownHandler}
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              className="mx-5"
            >
              <option>Select Currency</option>
              {results.map((result, index) => {
                return <option key={index}>{result}</option>;
              })}
            </select>
          </Col>
        </Row>
        <Row className="justify-content-md-center my-5 py-5">
          <Col sm={2}>
            <span> Date:</span>
          </Col>
          <Col sm={4}>
            <input
              onChange={dateHandler}
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              placeholder="format: yyyy-mm-dd"
            ></input>
          </Col>
          <Col sm={1}>
            <Button onClick={getTodayRate} variant="secondary" type="button">
              Convert
            </Button>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default Todayrate;
