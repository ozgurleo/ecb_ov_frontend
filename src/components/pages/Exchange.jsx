import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Image from "../repo/Image"

function Exchange() {
  const [results, setResults] = useState([]);
  const [amountToCurrency, setAmountToCurrency] = useState("");
  const [amountToEuro, setAmountToEuro] = useState("");
  const [optToCurrency, setOptToCurrency] = useState("");
  const [optInEuro, setOptInEuro] = useState("");
  const [resultGetToEuro, setResultGetToEuro] = useState("");
  const [resultGetInEuro, setResultGetInEuro] = useState("");
  const [date, setDate] = useState("");

  useEffect(() => {
    getResults();
  }, []);

  const getResults = async () => {
    const response = await axios.get("/getAllRatesEu/",{port:8081});
    const arrayResult = Object.keys(response.data.rates);
    setResults(arrayResult);
  };

  const inputHandler = event => {
    setAmountToCurrency(event.target.value);
  };

  const inputHandlerInEuro = event => {
    setAmountToEuro(event.target.value);
  };

  const dropDownHandler = event => {
    setOptToCurrency(event.target.value);
  };

  const dropDownHandlerInEuro = event => {
    setOptInEuro(event.target.value);
  };

  const dateHandler = event => {
    setDate(event.target.value);
  };

  const getToEuroHandler = async () => {
    if (date === "") {
      const response = await axios.get(
        `/getToEuro?currency=${optToCurrency}&amountToEuro=${amountToCurrency}`
      );
      setResultGetToEuro(response.data.amount_euros.toFixed(2));
    } else if (date !== "") {
      const response = await axios.get(
        `/getToEuro?currency=${optToCurrency}&amountToEuro=${amountToCurrency}&date=${date}`
      );
      setResultGetToEuro(response.data.amount_euros.toFixed(2));
    }
  };

  const getInEuroHandler = async () => {
    if (date === "") {
      const response = await axios.get(
        `/getInEuro?currency=${optInEuro}&amount=${amountToEuro}`
      );
      setResultGetInEuro(response.data.amount_currency.toFixed(2));
      
    } else if (date !== "") {
      const response = await axios.get(
        `/getInEuro?currency=${optInEuro}&amount=${amountToEuro}&date=${date}`
      );
      setResultGetInEuro(response.data.amount_currency.toFixed(2));
    }
  };

  return (
    <div>
      <Image />
      <h1 className="mx-5 my-5 text-center ">Convert value</h1>
      <Container className="py-5  justify-content-md-center">
        <Row className="justify-content-md-center">
          <Col sm={2}>
            <input
              onChange={inputHandlerInEuro}
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              placeholder="Enter amount"
            ></input>
          </Col>
          <Col sm={2}>
            <select
              onChange={dropDownHandlerInEuro}
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              className="mx-2"
            >
              <option>Select Currency</option>
              {results.map((result, index) => {
                return <option key={index}>{result}</option>;
              })}
            </select>
          </Col>
          <Col sm={2}>
            <span>{resultGetInEuro}</span>
          </Col>
          <Col sm={2}>
            <span>EUR</span>
          </Col>
          <Col sm={2}>
            <Button
              onClick={getInEuroHandler}
              variant="secondary"
              type="button"
            >
              Convert
            </Button>
          </Col>
        </Row>

        <Row className=" py-5  justify-content-md-center">
          <Col sm={2}>
            <input
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              placeholder="Enter amount"
              onChange={inputHandler}
            ></input>
          </Col>
          <Col sm={2}>
            <span className="mx-2">EUR</span>
          </Col>
          <Col sm={2}>
            <span style={{ textDecoration: "underline" }}>
              {resultGetToEuro}
            </span>
          </Col>

          <Col sm={2}>
            <select
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              onChange={dropDownHandler}
              className="mx-2"
            >
              <option>Select Currency</option>
              {results.map((result, index) => {
                return <option key={index}>{result}</option>;
              })}
            </select>
          </Col>
          <Col sm={2}>
            <Button
              onClick={getToEuroHandler}
              variant="secondary"
              type="button"
            >
              Convert
            </Button>
          </Col>
        </Row>
        <Row className=" py-5  justify-content-md-center">
          <Col style={{ textAlign: "center" }}>
            <span>date:</span>
            <input
              onChange={dateHandler}
              style={{ border: "none ", borderBottom: "2px solid grey" }}
              className="mx-2"
              placeholder="format: yyyy-mm-dd"
            ></input>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default Exchange;
