import React, { useState } from "react";
import OvChipCard from "./pages/OvChipCard"
import Exchange from "./pages/Exchange";
import Todayrate from "./pages/Todayrate";
import Avaragerate from "./pages/Avaragerate";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

function ControlledTabs() {
  return (
   
    <Router>
       <Navbar bg="primary" variant="light" className="justify-content-center">
        <Nav  className="justify-content-center" activeKey="/home">
          <Nav.Item>
            <Nav.Link href="/Exchange">Exchange</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href="/Todayrate">Today rate</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href="/Avaragerate">Avarage rate</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link href="/OvChip">Ov Chipcard</Nav.Link>
          </Nav.Item>
        </Nav>
    </Navbar>
      <div>
        <Switch>
          <Route exact path="/Exchange" component={Exchange} />
          <Route exact path="/Todayrate" component={Todayrate} />
          <Route exact path="/Avaragerate" component={Avaragerate} />
          <Route exact path="/OvChip" component={OvChipCard} />
        </Switch>
      </div>
      
    </Router>
  
  );
}

export default ControlledTabs;
